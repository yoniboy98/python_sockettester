import sys
import socket

from datetime import datetime

host = "192.168.1.25"

# translate to ipv4 and specify the ip address of your router in the terminal
# if len(sys.argv) == 2:
#   target = socket.gethostbyname(sys.argv[1])

# else:
#   print("invalid amount of arguments")
#   print("syntax: python3 scanner.py")
#   sys.exit()

print("-" * 50)
print("scanner target " + host)
print("time started " + str(datetime.now()))
print("-" * 50)

try:
    for port in range(50, 100):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        socket.setdefaulttimeout(1)
        result = s.connect_ex((host, port))
        print("checking port {}".format(port))
        if result == 0:
            print("port {} is open".format(port))
            s.close()

except KeyboardInterrupt:
    print("\n exiting program")
    sys.exit()

except socket.gaierror:
    print("hostname could not me resolved")
    sys.exit()

except socket.error:
    print("no connection to server")
    sys.exit()
